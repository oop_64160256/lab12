package com.nipitpon.week12;

import java.lang.reflect.Array;
import java.util.LinkedList;
import java.util.LinkedList;
import java.util.Collections;

public class TestLinkedList {
    public static void main(String[] args) {
        LinkedList<String> list = new LinkedList();
        list.add("A1");
        list.add("A2");
        list.add("A3");

        for (int i = 0; i < list.size(); i++) {
            System.out.println(list.get(i));
        }

        System.out.println();

        list.add(1, "A0");
        list.add("A3");
        for (String str : list) {
            System.out.println(str);
        }

        System.out.println();

        Object[] arr = list.toArray(new String[list.size()]);
        for (int i = 0; i < arr.length; i++) {
            System.out.println(arr[i]);
        }

        System.out.println();

        list.remove("A0");
        System.out.println(list);
        list.remove(3);
        System.out.println(list);

        list.set(1, "A0");
        System.out.println(list);
        System.out.println();


        //////////////////////////////

        LinkedList<String> list2 = new LinkedList(list);
        System.out.println(list2);
        list2.add("A4");
        System.out.println(list2);

        System.out.println();
        
        list.addAll(list2);
        System.out.println(list);

        System.out.println();

        System.out.println(list2.contains("A0"));
        System.out.println(list2.contains("A10"));
        System.out.println(list2.indexOf("A0"));
        System.out.println(list2.indexOf("A10"));

        System.out.println();

        Collections.sort(list);
        System.out.println(list);

        System.out.println();

        Collections.shuffle(list);
        System.out.println(list);

        System.out.println();

        Collections.reverse(list);
        System.out.println(list);

        System.out.println();

        Collections.swap(list, 0, 1);
        System.out.println(list);
    }
}
